'use strict';

// Make image from data-image
export function tabs() {
    $('.tabs-control > li').on('click', function(){
        let thisTabs = $(this).closest('.tabs-block'),
            thisTabId = $(this).data('tab'),
            thisTabItem = thisTabs.find('#' + thisTabId);

        $(this).addClass('active').siblings('li').removeClass('active');

        thisTabItem.addClass('active')
            .siblings('.tab-item')
            .removeClass('active');
    });
}
