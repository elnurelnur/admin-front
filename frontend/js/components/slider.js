'use strict';

export function adaptiveSlider(sliderName, toShow, toScroll) {

	$(sliderName).slick({
		dots: true,
		arrows: false,
		infinite: false,
		speed: 300,
		slidesToShow: toShow[0],
		slidesToScroll: toScroll[0],
		responsive: [
		  {
		    breakpoint: 1024,
		    settings: {
				slidesToShow: toShow[1],
				slidesToScroll: toScroll[1],
		      infinite: true,
		      dots: true
		    }
		  },
		  {
		    breakpoint: 768,
		    settings: {
		    	arrows: false,
				slidesToShow: toShow[2],
				slidesToScroll: toScroll[2]
		    }
		  },
		  {
		    breakpoint: 480,
		    settings: {
		    	arrows: false,
				slidesToShow: toShow[3],
				slidesToScroll: toScroll[3]
		    }
		  }
		  // You can unslick at a given breakpoint now by adding:
		  // settings: "unslick"
		  // instead of a settings object
		]
	});
}

export function simpleSlider(sliderName) {
    $(sliderName).slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: false
	});
}

export function sliderWithDots() {
    $(".slider-with-dots").slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
		arrows: false
	});
}

export function filterSlider() {

    $('.adaptive-slider-4').slick('slickFilter','.new-arrival-phone');

    $('.mobile-slider-filter > li').on('click', function(){
        if ($(this).hasClass('new-arrivals-btn')) {
            $('.adaptive-slider-4')
				.slick('slickUnfilter')
				.slick('slickFilter','.new-arrival-phone');

            $(this).addClass('active').siblings('li').removeClass('active');
        } else if($(this).hasClass('coming-soon-btn')) {
            $('.adaptive-slider-4')
				.slick('slickUnfilter')
				.slick('slickFilter','.coming-soon-phone');

            $(this).addClass('active').siblings('li').removeClass('active');
        }
    });
}
