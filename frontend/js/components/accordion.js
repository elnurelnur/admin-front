// ******* accordion
export function accordion() {
    // start accordion function
    $(".accordion-header").on("click", function() {

        let thisBody = $(this).closest('li').find(".accordion-body");


        thisBody.stop(false, false).slideToggle(300);
        $(this).toggleClass('active');
    });
}
