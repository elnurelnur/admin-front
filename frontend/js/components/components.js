'use strict';

// navigation mobile
export function mobileNavTrigger() {
    $('.nav-trigger').on("click", function() {
        $('.mobile-sidebar-section').addClass('active');
    });


    $('.mobile-sidebar-overlay').on("click", function() {
       $('.mobile-sidebar-section').removeClass('active');
    });
}

// navigation mobile
export function sidebarTrigger(trigger, component) {
    $(trigger).on("click", function() {
        $(component).toggleClass('active');
        $('.sidebar-overlay').toggleClass('active');
    });


    $('.sidebar-overlay').on("click", function() {
        $(this).removeClass('active');
        $(component).removeClass('active');
    });
}

// Add class to header when scrolled
export function headerScrolled(header) {
    $(window).scroll(function (e) {
        let height = $(window).height() - 30;

        if ($(window).scrollTop() >= height) {
            $(header).addClass('scrolled white-color');
        } else {
            $(header).removeClass('scrolled white-color');
        }
    });
}

export function closeOutside(thisItem, activeItem, itemBtn, itemBtnClass, addBtnClass) {
    $(document).mouseup(function (e) {
        let item = $(thisItem);

        if(item.siblings(itemBtn).is(e.target) || item.siblings(itemBtn).children().is(e.target)) {
            return false;
        } else if (!item.is(e.target) && item.has(e.target).length === 0) {
            item.removeClass(activeItem);
            $(itemBtn).removeClass(itemBtnClass).addClass(addBtnClass);
        }
    });
}

// Make image from data-image
export function imgBackground(imgClass) {
    $(imgClass).each(function () {
        let thisImg = $(this).data('img');

        $(this).css({
            'background': 'url(' + thisImg + '), #494949',
            'backgroundPosition': '50% 50%',
            'backgroundSize': 'cover'
        })
    });
}

// navigation mobile
export function signInMobile() {
    $('.mobile-sign-in').on("click", function() {
        $('.sign-in-sub-mobile').stop(false, false).slideToggle(300);
    });
}
