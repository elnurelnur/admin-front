'use strict';

export function dropdown() {
    $('.dropdown-btn').on('click', function(e){
        e.preventDefault();

        if($(this).hasClass('active')) {
            $('.dropdown-btn').removeClass('active');
            $('.dropdown-block').removeClass('active');
        } else {
            $(this).toggleClass('active');
            $(this).siblings('.dropdown-block').toggleClass('active');
        }

    });
}
