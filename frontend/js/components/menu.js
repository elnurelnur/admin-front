'use strict';

export function mobMenu() {

    $('.nav-hamburger').on('click', function(e) {
        e.preventDefault();

        let thisId = $(this).data('menu');

        $('#' + thisId).addClass('opened-menu');
    });

    $('.mobile-sidebar-overlay').on('click', function() {
       $('.menu-secr').removeClass('opened-menu');
    });

}