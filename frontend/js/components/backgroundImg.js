'use strict';

// Make image from data-image
export function imgBackground(imgClass) {
    $(imgClass).each(function(){
        let thisImg = $(this).data('img');

        $(this).css({
            'background': 'url(' + thisImg + ')',
            'backgroundPosition': '50% 50%',
            'backgroundSize': 'cover',
            'display': 'block'
        })
    });
}
