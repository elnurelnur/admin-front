'use strict';

export function mainPopup() {

    $('.popup-btn').on('click', function(e) {
        e.preventDefault();

        let thisId = $(this).data('popup');

        $('#' + thisId).addClass('opened-popup');
    });

    // popup overlay
    $('.popup-overlay').on('click', function() {
       $('.popup-wrapper').removeClass('opened-popup');
    });

    // close icon
    $('.close-icon').on("click", function() {
        $('.popup-wrapper').removeClass('opened-popup');
    });

}
