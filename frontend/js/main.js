'use strict';

// ******* import scripts
import {tabs} from './components/tabs';
import {mainPopup} from './components/popup';
import {dropdown} from './components/dropdown';
import {accordion} from './components/accordion';
import {makeTimer} from './components/makeTimer';
import {
    simpleSlider,
    sliderWithDots,
    adaptiveSlider
} from './components/slider';
import {
    closeOutside,
    mobileNavTrigger,
    sidebarTrigger,
    signInMobile,
    imgBackground
} from './components/components';

$(document).ready(function() {

    // ******* slide 7
    adaptiveSlider('.adaptive-is-7', [6, 6, 4, 1], [6, 6, 4, 1]);
    // ******* slide 3
    adaptiveSlider('.adaptive-is-3', [3, 3, 2, 1], [3, 2, 2, 1]);
    // ******* slide 2
    adaptiveSlider('.adaptive-is-2', [2, 2, 2, 1], [2, 2, 2, 1]);
    // ******* main Slider
    simpleSlider('.main-slider');
    // ******* sliderWithDots
    sliderWithDots();
    // ******* tabs
    tabs();
    // ******* imgBackground
    imgBackground('.img-bg');
    // ******* Close outside
    closeOutside('.dropdown-block.active', 'active', '.dropdown-btn.active', 'active', '');
    // ******* dropdown
    dropdown();
    // ******* mobileNavTrigger
    mobileNavTrigger();
    // ******* sidebarTrigger
    sidebarTrigger(".sidebar-right-trigger", ".sidebar-right-block");
    sidebarTrigger(".sidebar-left-trigger", ".sidebar-left-block");
    // ******* popup
    mainPopup();
    // ******* accordion
    accordion();
    // ******* signInMobile
    signInMobile();
    // ******* makeTimer
    setInterval(function() {
        makeTimer();
    }, 1000);
    // ******* Add placeholder to the dataTables_filter
    $('.dataTables_filter input').prop('placeholder', 'Search...');








    // ******* .lang-select
    $('.lang-select').selectric();
    // ******* .country-select
    $('.country-select').selectric({
        optionsItemBuilder: function(itemData) {
            return itemData.value.length ? '<img class="flag" src="img/flags/'+ itemData.value +'.png">' + '<span>' + itemData.text + '</span>' : itemData.text;
        }
    });
    // ******* .custom-select
    $('.custom-select > select').selectric();


    $('.custom-select .selectric').each(function(){

        let thisLabel = $(this).children('.label'),
            thisList = $(this).siblings('.selectric-items'),
            thisListItem = thisList.find('li'),
            disabled = thisList.find('.disabled').text();

        function changeColor() {
            if(thisLabel.text() === disabled) {
                thisLabel.css('color', '#757575');
            } else {
                thisLabel.css('color', '#032643');
            }
        }

        changeColor();

        thisListItem.on('click',function() {
            changeColor();
        });

    });

    // ******* search
    $('.search-icon').on("click", function() {
        $('.search-click').css({
            opacity: '1',
            visibility: 'visible',
            top: '100%',
            transition: 'all .3s ease'
        });
    });

    // ******* Search function
    $('.search-block').on('click', function(e) {
        e.preventDefault();

        $('.search-block-main').toggleClass('opened-search');

    });



    // tooltip
    $('.tooltip').tooltipster({
        theme: 'tooltipster-shadow'
    });





    document.addEventListener('click',toggleDropdown);

    // valideyn-e html- de data-target="" ve dr class-i, acilacaq blocka(target) data-dr="" ve drt class - i
    function toggleDropdown(e) {
        var _target = e.target;

        if(!_target.classList.contains('dr')) {
            _target = _target.parentNode;

			if(!_target.classList.contains('dr')) {
				return;
			}
        }

        var active = document.querySelector('.drt.active'),
            targetDr = document.querySelector('[data-dr=' + _target.dataset.target + ']'),
            icons;

        if (active && active != targetDr) {
            active.classList.remove('active');

            icons = document.querySelector('[data-target=' + active.dataset.dr + ']').getElementsByTagName('i');

            icons[0].classList.add('active');
            icons[1].classList.remove('active');
        }

		targetDr.classList.toggle('active');

		icons = _target.getElementsByTagName('i');

		icons[0].classList.toggle('active');
		icons[1].classList.toggle('active');
    }

    // slider images 
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });

    $('.slider-nav').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        infinite: false,
        arrows: false,
        centerMode: true,
        focusOnSelect: true
    });



    // select with search
    function formatState (state) {
        if (!state.id) {
          return state.text;
        }
        var baseUrl = "/user/pages/images/flags";
        var $state = $(
          '<span>' + state.text + '</span>'
        );
        return $state;
    };
      
    $(".js-example-templating").select2({
        templateSelection: formatState
    });



    // mob menu
    $('.menu_icon').on('click', function(){
        
        $('.mob_menu_sidebar').addClass('active');
        
    });


    $('.menu_icon_close').on('click', function(){
        
        $('.mob_menu_sidebar').removeClass('active');
        
    });



    $('.parent-container').magnificPopup({
        delegate: 'a', 
        type: 'image',
        gallery: {
            enabled: true
          }
        // other options
      });



    $.getJSON(
        'https://cdn.rawgit.com/highcharts/highcharts/057b672172ccc6c08fe7dbb27fc17ebca3f5b770/samples/data/usdeur.json',
        function (data) {

            Highcharts.chart('container', {
                chart: {
                    zoomType: 'x'
                },
                title: {
                    text: 'USD to EUR exchange rate over time'
                },
                subtitle: {
                    text: document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Exchange rate'
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: {
                                x1: 0,
                                y1: 0,
                                x2: 0,
                                y2: 1
                            },
                            stops: [
                                [0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                            ]
                        },
                        marker: {
                            radius: 2
                        },
                        lineWidth: 1,
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        },
                        threshold: null
                    }
                },

                series: [{
                    type: 'area',
                    name: 'USD to EUR',
                    data: data
                }]
            });
        }
    );


    $.getJSON(
        'https://cdn.rawgit.com/highcharts/highcharts/057b672172ccc6c08fe7dbb27fc17ebca3f5b770/samples/data/usdeur.json',
        function (data) {

            Highcharts.chart('container1', {
                chart: {
                    zoomType: 'x'
                },
                title: {
                    text: 'USD to EUR exchange rate over time'
                },
                subtitle: {
                    text: document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Exchange rate'
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: {
                                x1: 0,
                                y1: 0,
                                x2: 0,
                                y2: 1
                            },
                            stops: [
                                [0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                            ]
                        },
                        marker: {
                            radius: 2
                        },
                        lineWidth: 1,
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        },
                        threshold: null
                    }
                },

                series: [{
                    type: 'area',
                    name: 'USD to EUR',
                    data: data
                }]
            });
        }
    );


});
